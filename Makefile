destdir ?= 
prefix ?= /usr/local
exec_prefix ?= $(destdir)$(prefix)

bindir ?= $(exec_prefix)/bin
libexecdir ?= $(exec_prefix)/libexec
datarootdir ?= $(prefix)/share
datadir ?= $(datarootdir)
zsh_site_functions ?= $(datadir)/zsh/site-functions

all:
	@echo "Run make install to install"

install:
	install -d "$(bindir)"
	install -d "$(libexecdir)/ws-tools"
	install -d "$(zsh_site_functions)"

	install ./src/ws "$(bindir)"
	install ./src/edit "$(libexecdir)/ws-tools/"
	install ./src/open "$(libexecdir)/ws-tools/"

	install ./zsh-completions/_open "$(zsh_site_functions)/"
	install ./zsh-completions/_edit "$(zsh_site_functions)/"

uninstall:
	$(RM) "$(bindir)/ws"
	$(RM) -r "$(libexecdir)/ws-tools"
	$(RM) -r "$(zsh_site_functions)/_open"
	$(RM) -r "$(zsh_site_functions)/_edit"

.PHONY: all install uninstall
